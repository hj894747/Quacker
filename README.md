<h1 align="center"> Quacker </h1> <br>
<p align="center">
  <a href="https://github.com/TheHCJ/Quacker">
    <img alt="Quacker" title="Quacker" src="assets/icon.png" width="144">
  </a>
</p>

<p align="center">
  A fork of fritter that does so much more...
</p>

<p align="center">
  <a href="https://github.com/TheHCJ/Quacker/releases">
    <img src="https://raw.githubusercontent.com/flocke/andOTP/master/assets/badges/get-it-on-github.png"
       alt="Get it on F-Droid"
       height="80">
   </a>
</p>

## Features (From Fritter)
* Device-local subscriptions and groups
* Device-local saving of tweets, allowing offline reading
* Viewing profiles
* Viewing tweets, and tweet replies, threads and conversations
* Viewing and downloading pictures, videos and GIFs in tweets
* Searching for users
* Viewing trending topics
* Supports opening twitter.com links directly

## Features (From Quacker)
* Feed viewing within the app
* Searching for tweets
* Material You Design Language

## Screenshots
| <img alt="Viewing groups and subscriptions" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.jpg" width="200"/> | <img alt="Viewing a profile" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.jpg" width="200"/> | <img alt="Tweet cards" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.jpg" width="200"/> | <img alt="Viewing trends" src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.jpg" width="200"/> | <img alt="Viewing saved tweets" src="fastlane/metadata/android/en-US/images/phoneScreenshots/5.jpg" width="200"/> |

## Acknowledgments
Icon made with [OpenMoji](https://openmoji.org/)
